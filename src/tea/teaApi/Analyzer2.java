package tea.teaApi;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;


public class Analyzer2 {
	
	private boolean display;
	private String inputString;
	private int result;
	private double maxIntensity;
	private Vector<Data> bagOfWord;
	private Vector<Data> wordForAnalysis;
	private Vector<Double> coeffientOfEachWord;
	private int analysisStartIndex;
	private Vector<Intensity> intensityOfAnalyzedKeyword;
	private Vector<String> analyzedWords;
	
	//instance variable for handling database
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;
	
	private final String URL= "jdbc:mysql://localhost:3306/teadb";
	private final String USER= "root";
	private final String PASSWORD= "";
	
	public Analyzer2(String inputString, boolean display) {
		
		this.inputString=inputString;
		this.display= display;
		result=0;
		TempData.foundWordForAnalyze=false;
		
		if (display) {
			CalculationWindow.jTextArea.append(String.format("Input: "+inputString+"\n"));
		}
		
		EmoticonAnalyzer emoticonAnalyzer= new EmoticonAnalyzer(this.inputString,display);
		
		if (emoticonAnalyzer.isEmoFound()) {
			if (display) {
				CalculationWindow.jTextArea.append(String.format("Analyze finished\n"));
			}
			
			result= emoticonAnalyzer.getResult();
			maxIntensity=1.0;
			TempData.foundWordForAnalyze= true;
			//TODO edit-> add to training dataset
			
			//TODO change to current method
			//(new NaiveBayes(inputString, result, 60)).process();//intensity 0.60 default
		}
		else {
			bagOfWord= (new POStagger(this.inputString)).getTaggedInfo();
			if (display) {
				CalculationWindow.jTextArea.append(String.format("No emoticon found:\n\n"));
				CalculationWindow.jTextArea.append(String.format("Sentence Analyzer:\n"));
				CalculationWindow.jTextArea.append(String.format("Initial Word list:\n"));
				CalculationWindow.jTextArea.append(String.format("%-20s %-20s\n","word","base word"));
				CalculationWindow.jTextArea.append(String.format("________________________________________\n"));
				for (Data words : bagOfWord) {
					CalculationWindow.jTextArea.append(String.format("%-20s ",words.getMainWord()));
					CalculationWindow.jTextArea.append(String.format("%-20s\n",words.getBaseWord()));
				}
				CalculationWindow.jTextArea.append(String.format("\n"));
			}
			
			sentenceLevelAnalysis();
			phraseLevelAnalysis();
			wordLevelAnalysis();
			
			calculateResult();
			
//			for (Data data : wordForAnalysis) {
//				System.out.println(data.getBaseWord());
//			}
		}
		
		
	}
	
	public int getResult() {
		return result;
	}
	
	public double getMaxIntensity() {
		return maxIntensity;
	}
	
	private void sentenceLevelAnalysis() {

		analysisStartIndex =0;
		for (int i = 0; i < bagOfWord.size(); i++) {
			if (bagOfWord.elementAt(i).getBaseWord().equals("but")) {
				analysisStartIndex=i+1;
				break;
			}
		}
		
		if (display) {
			CalculationWindow.jTextArea.append(String.format("\nSentence level analysis:\n"));
			CalculationWindow.jTextArea.append(String.format("\n%20s\t\tAnger \tDisgust\tFear \tGuilt\tInterest Joy \tSadness\tShame \tSurprise\n"," "));
			CalculationWindow.jTextArea.append(String.format("____________________"));//20 space
			CalculationWindow.jTextArea.append(String.format("____________________"));//20 space
			CalculationWindow.jTextArea.append(String.format("____________________"));//20 space
			CalculationWindow.jTextArea.append(String.format("____________________"));//20 space
			CalculationWindow.jTextArea.append(String.format("____________________"));//20 space
			CalculationWindow.jTextArea.append(String.format("____________________"));//20 space
			CalculationWindow.jTextArea.append(String.format("____________________\n"));//20 space
			
			CalculationWindow.jTextArea.append(String.format("\nWord list for analysis: \n"));
			
			for (int i = analysisStartIndex; i < bagOfWord.size(); i++) {
				CalculationWindow.jTextArea.append(String.format("%-20s ",bagOfWord.elementAt(i).getBaseWord()));
				double tempCoefficient= getCoefficient(bagOfWord.elementAt(i).getBaseWord());
				if (tempCoefficient!=-1.00) {
					CalculationWindow.jTextArea.append(String.format("\t\t"+tempCoefficient));
					CalculationWindow.jTextArea.append(String.format("(coefficient)\n"));
				}
				else {
					Intensity tempIntensity= getIntensity(bagOfWord.elementAt(i).getBaseWord());
					if (tempIntensity!=null) {
						CalculationWindow.jTextArea.append(String.format("\t\t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \n",
								tempIntensity.getAnger(),tempIntensity.getDisgust(),
								tempIntensity.getFear(), tempIntensity.getGuilt(),
								tempIntensity.getInterest(),tempIntensity.getJoy(),
								tempIntensity.getSadness(),tempIntensity.getShame(),tempIntensity.getSurprise()));
					}
					else {
						CalculationWindow.jTextArea.append(String.format("\t\t0.00 \t0.00 \t0.00 \t0.00 \t0.00 \t0.00 \t0.00 \t0.00 \t0.00\n"));
					}
				}
				
			}
		}
		
	}

	private void phraseLevelAnalysis() {
		
		wordForAnalysis = new Vector<Data>();
		coeffientOfEachWord = new Vector<Double>();
		
		analyzedWords= new Vector<String>();

		double coefficient = 1.0;
		String temp="";

		for (int i = analysisStartIndex; i < bagOfWord.size(); i++) {
			Intensity tempIntensity = getIntensity(bagOfWord.elementAt(i).getBaseWord());
			if (tempIntensity!=null) {
				wordForAnalysis.addElement(bagOfWord.elementAt(i));
				coeffientOfEachWord.addElement(coefficient);
				temp+= bagOfWord.elementAt(i).getBaseWord();
				analyzedWords.addElement(temp);
				TempData.foundWordForAnalyze=true;
				// coefficient=1.0;
			}
			
			double tempCoefficient= getCoefficient(bagOfWord.elementAt(i).getBaseWord());
			if (tempCoefficient!=-1.00) {
				coefficient = tempCoefficient;
				temp= bagOfWord.elementAt(i).getBaseWord()+" ";
			} else {
				coefficient = 1.0;
				temp="";
			}
		}
	}
	
	private void wordLevelAnalysis() {
		
		if (display) {
			CalculationWindow.jTextArea.append(String.format("\nIntensity of words and phrase:\n"));
		}

		intensityOfAnalyzedKeyword = new Vector<Intensity>();
		for (int i = 0; i < wordForAnalysis.size(); i++) {

			Intensity tempIntensity= getIntensity(wordForAnalysis.elementAt(i).getBaseWord());
			if (tempIntensity==null) {
				tempIntensity= new Intensity();//with default value 0.00
			}
			
			Intensity intensity = calculateIntensityMultiplyCoefficient(tempIntensity,coeffientOfEachWord.elementAt(i));

			intensityOfAnalyzedKeyword.addElement(intensity);
			
			if (display) {
				CalculationWindow.jTextArea.append(String.format("%-20s \t\t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f \t%.2f\n",
						analyzedWords.elementAt(i),
						intensity.getAnger(),intensity.getDisgust(),
						intensity.getFear(), intensity.getGuilt(),
						intensity.getInterest(),intensity.getJoy(),
						intensity.getSadness(),intensity.getShame(),intensity.getSurprise()));
			}

		}

	}

	private Intensity calculateIntensityMultiplyCoefficient(
			Intensity intensity, double coefficient) {

		if (coefficient == 1.0) {
			return intensity;
		} else if (coefficient > 0.0) {
			Intensity ret = new Intensity();

			ret.setAnger(intensity.getAnger() * coefficient);
			ret.setDisgust(intensity.getDisgust() * coefficient);
			ret.setFear(intensity.getFear() * coefficient);
			ret.setGuilt(intensity.getGuilt() * coefficient);
			ret.setInterest(intensity.getInterest() * coefficient);
			ret.setJoy(intensity.getJoy() * coefficient);
			ret.setSadness(intensity.getSadness() * coefficient);
			ret.setShame(intensity.getShame() * coefficient);
			ret.setSurprise(intensity.getSurprise() * coefficient);

			return ret;
		} else if (coefficient <= 0.0) {

			Intensity ret = new Intensity(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
			return ret;
		}
		return intensity;
	}
	
	//return the intensity of the word from the database
	//return null if the word is not found in the database
	private Intensity getIntensity(String word) {
		Intensity intensityTEAdb= null;
		try {
			//connection= DriverManager.getConnection(URL, USER, PASSWORD);
			connection = ConnectionClassTEAapi.getTEAConnection();
			String query= "SELECT W.anger, W.disgust, W.fear, W.guilt, "
					+ " W.interest, W.joy, W.sadness, W.shame, W.surprise"
					+ " FROM WORD W WHERE W.word= ?";
			preparedStatement= connection.prepareStatement(query);
			preparedStatement.setString(1, word);
			resultSet= preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				intensityTEAdb= new Intensity();
				intensityTEAdb.setAnger(resultSet.getDouble(1));
				intensityTEAdb.setDisgust(resultSet.getDouble(2));
				intensityTEAdb.setFear(resultSet.getDouble(3));
				intensityTEAdb.setGuilt(resultSet.getDouble(4));
				intensityTEAdb.setInterest(resultSet.getDouble(5));
				intensityTEAdb.setJoy(resultSet.getDouble(6));
				intensityTEAdb.setSadness(resultSet.getDouble(7));
				intensityTEAdb.setShame(resultSet.getDouble(8));
				intensityTEAdb.setSurprise(resultSet.getDouble(9));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Intensity intensityETLdb= null;
		try {
			//connection= DriverManager.getConnection(URL, USER, PASSWORD);
			connection = ConnectionClassETL.getETLConnection();
			String query= "SELECT W.anger, W.disgust, W.fear, W.guilt, "
					+ " W.interest, W.joy, W.sadness, W.shame, W.surprise"
					+ " FROM nbintensity W WHERE W.word= ?";
			preparedStatement= connection.prepareStatement(query);
			preparedStatement.setString(1, word);
			resultSet= preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				intensityETLdb= new Intensity();
				intensityETLdb.setAnger(resultSet.getDouble(1));
				intensityETLdb.setDisgust(resultSet.getDouble(2));
				intensityETLdb.setFear(resultSet.getDouble(3));
				intensityETLdb.setGuilt(resultSet.getDouble(4));
				intensityETLdb.setInterest(resultSet.getDouble(5));
				intensityETLdb.setJoy(resultSet.getDouble(6));
				intensityETLdb.setSadness(resultSet.getDouble(7));
				intensityETLdb.setShame(resultSet.getDouble(8));
				intensityETLdb.setSurprise(resultSet.getDouble(9));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			//close the connections
			resultSet.close();
			preparedStatement.close();
			//connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (intensityTEAdb==null && intensityETLdb==null) {
			return null;
		}
		else if (intensityTEAdb==null && intensityETLdb!=null) {
			return intensityETLdb;
		}
		else if (intensityTEAdb!=null && intensityETLdb==null) {
			return intensityTEAdb;
		}
		else if (intensityTEAdb!=null && intensityETLdb!=null) {
			//take 60% value of TEAdb and 40% value of ETLdb
			intensityTEAdb = intensityTEAdb.convertIntensity(60);
			intensityETLdb = intensityETLdb.convertIntensity(40);
			Intensity total = Intensity.add(intensityTEAdb, intensityETLdb);
			return total;
		}

		
		return null;
	}
	
	
	//return the coefficient from the database
	//if the coefficient is not found then return -1.00
	private double getCoefficient(String coefficient) {
		double ret= -1.00;
		try {
			//connection= DriverManager.getConnection(URL, USER, PASSWORD);
			
			connection = ConnectionClassTEAapi.getTEAConnection();
			String query = "select c.value from coefficient c where c.coefficient= ?";
			preparedStatement= connection.prepareStatement(query);
			preparedStatement.setString(1, coefficient);
			resultSet= preparedStatement.executeQuery();
			if (resultSet.next()) {
				ret= resultSet.getDouble(1);
			}
			
			//close the connections
			resultSet.close();
			preparedStatement.close();
		//	connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return ret;
	}


	private void calculateResult() {
		double totalAnger = 0.0;
		double totalDisgust = 0.0;
		double totalFear = 0.0;
		double totalGuilt = 0.0;
		double totalInterest = 0.0;
		double totalJoy = 0.0;
		double totalSadness = 0.0;
		double totalShame = 0.0;
		double totalSurprise = 0.0;
		
		double maxAnger = 0.0;
		double maxDisgust = 0.0;
		double maxFear = 0.0;
		double maxGuilt = 0.0;
		double maxInterest = 0.0;
		double maxJoy = 0.0;
		double maxSadness = 0.0;
		double maxShame = 0.0;
		double maxSurprise = 0.0;
		

		for (int i = 0; i < intensityOfAnalyzedKeyword.size(); i++) {
			Intensity intensity = intensityOfAnalyzedKeyword.elementAt(i);

			totalAnger += intensity.getAnger();
			maxAnger= Math.max(intensity.getAnger(), maxAnger);
			totalDisgust += intensity.getDisgust();
			maxDisgust= Math.max(intensity.getDisgust(), maxDisgust);
			totalFear += intensity.getFear();
			maxFear= Math.max(intensity.getFear(), maxFear);
			totalGuilt += intensity.getGuilt();
			maxGuilt= Math.max(intensity.getGuilt(), maxGuilt);
			totalInterest += intensity.getInterest();
			maxInterest= Math.max(intensity.getInterest(), maxInterest);
			totalJoy += intensity.getJoy();
			maxJoy= Math.max(intensity.getJoy(), maxJoy);
			totalSadness += intensity.getSadness();
			maxSadness= Math.max(intensity.getSadness(), maxSadness);
			totalShame += intensity.getShame();
			maxShame= Math.max(intensity.getShame(), maxShame);
			totalSurprise += intensity.getSurprise();
			maxSurprise= Math.max(intensity.getSurprise(), maxSurprise);

		}
		
		if (display) {
			CalculationWindow.jTextArea.append(String.format("\n%-20s \t\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n",
					"Total", totalAnger, totalDisgust, totalFear, totalGuilt, totalInterest,
					totalJoy, totalSadness, totalShame, totalSurprise));
		}

		double totalPos = 0.0;
		double totalNeg = 0.0;
		int emoPos = 0;
		int emoNeg = 0;
		double maxPos = 0.0;
		double maxNeg = 0.0;
		maxIntensity=0.0;

		if (totalJoy > totalPos) {
			totalPos = totalJoy;
			emoPos = Constant.JOY;
			maxPos= maxJoy;
		}

		if (totalInterest > totalPos) {
			totalPos = totalInterest;
			emoPos = Constant.INTEREST;
			maxPos= maxInterest;
		}

		if (totalSurprise > totalPos) {
			totalPos = totalSurprise;
			emoPos = Constant.SURPRISE;
			maxPos= maxSurprise;
		}

		if (totalSadness > totalNeg) {
			totalNeg = totalSadness;
			emoNeg = Constant.SADNESS;
			maxNeg= maxSadness;
		}

		if (totalAnger > totalNeg) {
			totalNeg = totalAnger;
			emoNeg = Constant.ANGER;
			maxNeg= maxAnger;
		}

		if (totalDisgust > totalNeg) {
			totalNeg = totalDisgust;
			emoNeg = Constant.DISGUST;
			maxNeg= maxDisgust;
		}

		if (totalFear > totalNeg) {
			totalNeg = totalFear;
			emoNeg = Constant.FEAR;
			maxNeg= maxFear;
		}

		if (totalGuilt > totalNeg) {
			totalNeg = totalGuilt;
			emoNeg = Constant.GUILT;
			maxNeg= maxGuilt;
		}
		if (totalShame > totalNeg) {
			totalNeg = totalShame;
			emoNeg = Constant.SHAME;
			maxNeg= maxShame;
		}

		//
		if (totalPos == 0.0 && totalNeg == 0.0) {
			//result += differentTypeOfEmo[NUTRAL];
			result = Constant.NEUTRAL;
		} else if (totalPos == totalNeg) {
			result = Constant.MIXED;
			maxIntensity= Math.max(maxPos, maxNeg);
		} else if (totalPos > totalNeg) {
			result = emoPos;
			maxIntensity= maxPos;
		} else if (totalNeg > totalPos) {
			result =emoNeg;
			maxIntensity= maxNeg;
		}

	}
	
	
	


}
