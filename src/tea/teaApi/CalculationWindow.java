package tea.teaApi;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;



public class CalculationWindow extends JFrame {
	
	public static JTextArea jTextArea= new JTextArea();
	private JScrollPane jScrollPane= new JScrollPane(jTextArea);
	
	public CalculationWindow(String inputString) {
		
		setTitle("Result Calculation Procedure");
		setSize(1040,580);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		jTextArea.setWrapStyleWord(true);
		jTextArea.setFont(new Font("monospaced", Font.PLAIN, 12));
		jTextArea.setEditable(false);
		jScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		jTextArea.setText("");
		new Analyzer(inputString, true);
		
		
		add(jScrollPane);
		
	}

}
