package tea.teaApi;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;


public class ChatBoxFrame extends JFrame{

	private JLabel chatboxJLabel;
	public StyledDocument document;
	SimpleAttributeSet simpleAttributeSet;
	SimpleAttributeSet resultAttributeSet;
	private JTextPane chatHistory;
	private JButton analyzeButton;
	private JScrollPane chatboxJScrollPane;
	private String chatboxString;
	
	private JLabel user1JLabel;
	private JTextArea user1ChatBox;
	private JButton user1SendButton;
	private JScrollPane user1JScrollPane;
	private String user1String;
	
	private JLabel user2JLabel;
	private JTextArea user2ChatBox;
	private JButton user2SendButton;
	private JScrollPane user2JScrollPane;
	private String user2String;
	
	public ChatBoxFrame() {
		
		super("SenTex");
		
		setLayout(new GridLayout(2,1));
		
		document= new DefaultStyledDocument();
		
		simpleAttributeSet= new SimpleAttributeSet();
		
		resultAttributeSet= new SimpleAttributeSet();
		resultAttributeSet.addAttribute(StyleConstants.CharacterConstants.Foreground, Color.RED);
		
		chatboxJLabel= new JLabel("Chat box   ");
		try {
			document.insertString(document.getLength(), "", simpleAttributeSet);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		chatHistory= new JTextPane(document);
		chatHistory.setEditable(false);
		chatHistory.setSize(100, 100);
//		chatHistory.setBounds(0, 0, 200, 200);
		chatboxJScrollPane= new JScrollPane(chatHistory,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		analyzeButton= new JButton("Analyze");
		chatboxString= new String();
		
		user1JLabel= new JLabel("User 1: ");
		user1ChatBox= new JTextArea(5,40);
		user1JScrollPane= new JScrollPane(user1ChatBox, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		user1SendButton= new JButton("Send \nEmoticons");
		user1String= new String("Neutral");
		
		user2JLabel= new JLabel("User 2: ");
		user2ChatBox= new JTextArea(5,40);
		user2JScrollPane= new JScrollPane(user2ChatBox, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		user2SendButton= new JButton("Send \nEmoticons");
		user2String= new String("Neutral");
		
		String spaceString="                                                                                                                                                                                                   ";
		
		add(chatboxJScrollPane);
		
		JPanel panel= new JPanel();
		panel.setLayout(new FlowLayout());
		
		panel.add(analyzeButton);
		panel.add(new JLabel(spaceString));
		
		panel.add(user1JLabel);
		panel.add(user1JScrollPane);
		panel.add(user1SendButton);
		panel.add(new JLabel(spaceString));
		
		panel.add(user2JLabel);
		panel.add(user2JScrollPane);
		panel.add(user2SendButton);
		//add(new JLabel(spaceString));
		
		add(panel);
		
		//edit
		user1ChatBox.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
			
				char ch= arg0.getKeyChar();
				
				if (ch== KeyEvent.VK_ENTER) {
					String temp= user1ChatBox.getText().toString();
					user1ChatBox.setText("");
					try {
						document.insertString(document.getLength(), "User 1: "+temp+"\n", simpleAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					
					temp= Integer.toString((new SenTexProcess(temp)).getResult());
					try {
						document.insertString(document.getLength(), "[[Emotion of the text : "+temp+"]]\n", resultAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					
					StyleContext context= new StyleContext();
					Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
					
					String imageName= temp.toLowerCase();
					if (imageName.equals("mixed")) {
						imageName= new String("neutral");
					}
					
					Icon img= new ImageIcon(new String("images/"+imageName+".jpg"));
					JLabel label= new JLabel(img);
					StyleConstants.setComponent(labelStyle, label);
					
					try {
						document.insertString(document.getLength(), "image of "+imageName, labelStyle);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					try {
						document.insertString(document.getLength(), "\n\n", resultAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}

					
				}

		
			}
				
		});
		
		
		
		user1SendButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
			
				EmoticonsFrame emoticonsFrame = new EmoticonsFrame("User 1");
				//emoticonsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				emoticonsFrame.setSize(300,300);
				emoticonsFrame.setVisible(true);
				
			}
		});
		
		
		
		
		/*user1SendButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String temp= user1ChatBox.getText().toString();
				user1ChatBox.setText("");
				try {
					document.insertString(document.getLength(), "User 1: "+temp+"\n", simpleAttributeSet);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				
				temp= (new SenTexProcess(temp)).getResult();
				try {
					document.insertString(document.getLength(), "[[Emotion of the text : "+temp+"]]\n", resultAttributeSet);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				
				StyleContext context= new StyleContext();
				Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
				
				String imageName= temp.toLowerCase();
				if (imageName.equals("mixed")) {
					imageName= new String("neutral");
				}
				
				Icon img= new ImageIcon(new String("images/"+imageName+".jpg"));
				JLabel label= new JLabel(img);
				StyleConstants.setComponent(labelStyle, label);
				
				try {
					document.insertString(document.getLength(), "image of "+imageName, labelStyle);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				try {
					document.insertString(document.getLength(), "\n\n", resultAttributeSet);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}

				
			}
		}); */
		
		
		
		
		
		user2ChatBox.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
			
				char ch= arg0.getKeyChar();
				
				if (ch== KeyEvent.VK_ENTER) {
					String temp= user2ChatBox.getText().toString();
					user2ChatBox.setText("");
					try {
						document.insertString(document.getLength(), "User 2: "+temp+"\n", simpleAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					
					temp= Integer.toString((new SenTexProcess(temp)).getResult());
					try {
						document.insertString(document.getLength(), "[[Emotion of the text : "+temp+"]]\n", resultAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					
					StyleContext context= new StyleContext();
					Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
					
					String imageName= temp.toLowerCase();
					if (imageName.equals("mixed")) {
						imageName= new String("neutral");
					}
					
					Icon img= new ImageIcon(new String("images/"+imageName+".jpg"));
					JLabel label= new JLabel(img);
					StyleConstants.setComponent(labelStyle, label);
					
					try {
						document.insertString(document.getLength(), "image of "+imageName, labelStyle);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					try {
						document.insertString(document.getLength(), "\n\n", resultAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}

					
				}

		
			}
				
		});

		
		
		
		
		user2SendButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
			
				EmoticonsFrame emoticonsFrame = new EmoticonsFrame("User 2");
				//emoticonsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				emoticonsFrame.setSize(300,300);
				emoticonsFrame.setVisible(true);
				
			}
		});
		
		
		
		
		
		
		
		
		
		
		/*user2SendButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String temp= user2ChatBox.getText().toString();
				user2ChatBox.setText("");
				try {
					document.insertString(document.getLength(), "User 2: "+temp+"\n", simpleAttributeSet);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				
				temp= (new SenTexProcess(temp)).getResult();
				try {
					document.insertString(document.getLength(), "[[Emotion of the text : "+temp+"]]\n", resultAttributeSet);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				
				StyleContext context= new StyleContext();
				Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
				
				String imageName= temp.toLowerCase();
				if (imageName.equals("mixed")) {
					imageName= new String("neutral");
				}
				
				Icon img= new ImageIcon(new String("images/"+imageName+".jpg"));
				JLabel label= new JLabel(img);
				StyleConstants.setComponent(labelStyle, label);
				
				try {
					document.insertString(document.getLength(), "image of "+imageName, labelStyle);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				try {
					document.insertString(document.getLength(), "\n\n", resultAttributeSet);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}

						
			}
		}); */
		
		analyzeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String temp= "Next probable sentiment of user 1 : "+user1String+"\nNext prabable sentiment of user 2 : "+user2String+"\n";
				
				JOptionPane.showMessageDialog(ChatBoxFrame.this,temp );
				
			}
		});
		
	}
	
	private class EmoticonsFrame extends JFrame {

		public String userName;
		
		final int ROW=4;
		final int COL=5;
		
		JButton imageButton[][];
		
		Map<JButton, String> mapEmoName;
		public EmoticonsFrame(String userNameParameter) {
			
			super("Emoticons");
			
			this.userName=userNameParameter;
			
			setLayout(new GridLayout(ROW,COL));
			
			mapEmoName= new HashMap<JButton,String>();
			
			imageButton= new JButton [ROW][COL];
			String imagePath="emoticons/";
			for (int i = 0; i < ROW; i++) {
				for (int j = 0; j < COL; j++) {
					String temp=imagePath+Integer.toString(i)+Integer.toString(j)+".jpg";
					Icon emoIcon=new ImageIcon(temp);
					imageButton[i][j]=new JButton("",emoIcon);
					mapEmoName.put(imageButton[i][j], temp);
					add(imageButton[i][j]);
					imageButton[i][j].addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent actionEvent) {
							
							String imageNameString= mapEmoName.get(actionEvent.getSource());
							//System.out.println(imageNameString);
							try {
								document.insertString(document.getLength(), String.format("%s: ", userName) ,simpleAttributeSet);
							} catch (BadLocationException e) {
								e.printStackTrace();
							}
							StyleContext context= new StyleContext();
							Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
							
							Icon img= new ImageIcon(imageNameString);
							JLabel label= new JLabel(img);
							StyleConstants.setComponent(labelStyle, label);
							
							try {
								document.insertString(document.getLength(), imageNameString, labelStyle);
							} catch (BadLocationException e) {
								e.printStackTrace();
							}
							try {
								document.insertString(document.getLength(), "\n\n", resultAttributeSet);
							} catch (BadLocationException e) {
								e.printStackTrace();
							}

							setVisible(false);
							
						}
					});
				}
			}
			
		}
		
	}

	
}
