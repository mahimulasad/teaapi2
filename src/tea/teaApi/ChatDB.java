package tea.teaApi;
import java.util.Vector;


public class ChatDB {
	
	static Vector<Integer> chatUser;
	static Vector<Integer> chatEmo;
	static int user1Count;
	static int user2Count;
	
	public ChatDB() {
		chatUser= new Vector<Integer>();
		chatEmo= new Vector<Integer>();
		user1Count=user2Count=0;
	}
	
	public static void addEmoToDB(int user, String imageName){
		addToDB(user, (new EmotionOfImage()).getEmotionOfTheImage(imageName));
	}
	
	
	
	public static void addSentenceToDB(int user, String inputSentence) {
		addToDB(user, (new SenTexProcess(inputSentence)).getResult());
	}
	
	private static void addToDB(int user, int chatagory) {
		if (chatUser.size()==0) {
			chatUser.addElement(user);
			chatEmo.addElement(chatagory);
			count(user);
		}
		else if (chatUser.elementAt(chatUser.size()-1) ==user) {
			chatEmo.remove(chatEmo.size()-1);
			chatEmo.addElement(chatagory);
		}
		else {
			chatUser.addElement(user);
			chatEmo.addElement(chatagory);
			count(user);
		}
	}
	
	private static void count(int user){
		if (user==1) {
			user1Count++;
		}
		else {
			user2Count++;
		}
	}

}
