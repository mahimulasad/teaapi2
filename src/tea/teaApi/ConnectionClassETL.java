package tea.teaApi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionClassETL {
	
	private final String urlETL= "jdbc:mysql://localhost:3306/etl";
	private final String user= "root";
	private final String password= "";
	
	public static Connection connectionETL;
	
	public ConnectionClassETL() {
		try {
			connectionETL = DriverManager.getConnection(urlETL, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getETLConnection(){
		return connectionETL;
	}

}
