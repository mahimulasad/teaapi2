package tea.teaApi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionClassTEAapi {
	
	private final String urlETL= "jdbc:mysql://localhost:3306/teadb";
	private final String user= "root";
	private final String password= "";
	
	public static Connection connectionTEA;
	
	public ConnectionClassTEAapi() {
		try {
			connectionTEA = DriverManager.getConnection(urlETL, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getTEAConnection() {
		return connectionTEA;
	}

	
}
