package tea.teaApi;

public class Constant {

	public static String differentTypeOfEmo[] = { "Neutral", "Anger", "Disgust",
			"Fear", "Guilt", "Interest", "Joy", "Sad", "Shame", "Surprise",
			"Mixed" };

	public static final int NEUTRAL = 0;
	public static final int ANGER = 1;
	public static final int DISGUST = 2;
	public static final int FEAR = 3;
	public static final int GUILT = 4;
	public static final int INTEREST = 5;
	public static final int JOY = 6;
	public static final int SADNESS = 7;
	public static final int SHAME = 8;
	public static final int SURPRISE = 9;
	public static final int MIXED = 10;
	
	public static String NBacceptedtagSet[]= {"JJ", "JJR", "JJS", "NN", "NNS", "NNP", "NNPS",
			"RB", "RBR", "RBS", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ"};


}
