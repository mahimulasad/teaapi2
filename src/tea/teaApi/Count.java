package tea.teaApi;

public class Count {
	
	public int anger;
	public int disgust;
	public int fear;
	public int guilt;
	public int interest;
	public int joy;
	public int sadness;
	public int shame;
	public int surprise;
	
	public Count() {
		anger= disgust= fear= guilt= interest= joy= sadness= shame= surprise=0;
	}
	
	public Count(int anger, int disgust, int fear, int guilt, int interest,
			int joy, int sadness, int shame, int surprise){
		this.anger = anger;
		this.disgust = disgust;
		this.fear = fear;
		this.guilt = guilt;
		this.interest = interest;
		this.joy = joy;
		this.sadness = sadness;
		this.shame = shame;
		this.surprise = surprise;
	}
	
	public Count(Count temp){
		anger= temp.anger;
		disgust= temp.disgust;
		fear= temp.fear;
		guilt= temp.guilt;
		interest= temp.interest;
		joy= temp.joy;
		sadness= temp.sadness;
		shame= temp.shame;
		surprise= temp.surprise;
	}
	
	public int totalCount(){
		return (anger+disgust+fear+guilt+interest+joy+sadness+shame+surprise);
	}
	
	public void increaseAnger() {
		anger++;
	}
	
	public void increaseDisgust() {
		disgust++;
	}
	
	public void increaseFear() {
		fear++;
	}

	public void increaseGuilt() {
		guilt++;
	}

	public void increaseInterest() {
		interest++;
	}

	public void increaseJoy() {
		joy++;
	}

	public void increaseSadness() {
		sadness++;
	}

	public void increaseShame() {
		shame++;
	}

	public void increaseSurprise() {
		surprise++;
	}

	public void increaseAllAppropriate(boolean isAnger, boolean isDisgust, boolean isFear, boolean isGuilt,
			boolean isInterest, boolean isJoy, boolean isSadness, boolean isShame, boolean isSurprise) {
		if (isAnger) {
			increaseAnger();
		}
		if (isDisgust) {
			increaseDisgust();
		}
		if (isFear) {
			increaseFear();
		}
		if (isGuilt) {
			increaseGuilt();
		}
		if (isInterest) {
			increaseInterest();
		}
		if (isJoy) {
			increaseJoy();
		}
		if (isSadness) {
			increaseSadness();
		}
		if (isShame) {
			increaseShame();
		}
		if (isSurprise) {
			increaseSurprise();
		}
	}
}
