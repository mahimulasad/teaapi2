package tea.teaApi;

public class Data {
	
	private String mainWord;
	private String pos;
	private String baseWord;
	
	public Data() {
		
	}

	public String getMainWord() {
		return mainWord;
	}

	public void setMainWord(String mainWord) {
		this.mainWord = mainWord;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getBaseWord() {
		return baseWord;
	}

	public void setBaseWord(String baseWord) {
		this.baseWord = baseWord;
	}
	
	public String toString() {
		return this.mainWord+"["+this.baseWord+"] ->"+this.pos;
	}

}
