package tea.teaApi;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;
import java.util.Vector;


public class EmoticonAnalyzer {
	
	private String inputString;
	private boolean display;
	private Vector<String> word;
	private boolean emoFound;
	private int result;
	
	//instance variable for handling database
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;
	
	private final String URL= "jdbc:mysql://localhost:3306/teadb";
	private final String USER= "root";
	private final String PASSWORD= "";
	
	
	public EmoticonAnalyzer(String inputString, boolean display) {
		
		this.inputString=inputString;
		this.display= display;
		emoFound=false;
		result=0;
		if (display) {
			CalculationWindow.jTextArea.append(String.format("Emoticon and Abbreviation Analyzer:\n"));
		}
		sentenceParsing();
		calculateResult();
		
	}
	
	private void sentenceParsing() {

		word = new Vector<String>();

		StringTokenizer stringTokenizer = new StringTokenizer(inputString);
		String tokenizeWord;
		while (stringTokenizer.hasMoreTokens()) {
			tokenizeWord = stringTokenizer.nextToken();
			// tokenizeWord=tokenizeWord.toLowerCase();//emo will be changed if we make lowercase
			tokenizeWord = removePunctuation(tokenizeWord);
			word.addElement(tokenizeWord);
		}
	}

	// remove extra punctuation from the tail of the string
	private String removePunctuation(String str) {

		if (isEmo(str)) {// if the string is a emo, no need to remove anything
			return str;
		} else {
			str = str.toLowerCase();
			int len = str.length();
			for (int i = len - 1; i >= 0; i--) {
				if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
					return str.substring(0, i + 1);
				}
			}
		}
		return "";
	}

	// returns if the string is a emo or not
	private boolean isEmo(String str) {

		if (str.length() == 1) {// is not a emo if its length is 1
			return false;
		}
		str = str.toLowerCase();
		// if any of the 1st two character is not alphabet, then it's a emo
		if (!(str.charAt(0) >= 'a' && str.charAt(0) <= 'z')
				|| !(str.charAt(1) >= 'a' && str.charAt(1) <= 'z')) {
			return true;
		}
		return false;
	}

	private void calculateResult() {
		for (int i = word.size() - 1; i >= 0; i--) {
			int tempEmoType= getEmoType(word.elementAt(i));
			if (tempEmoType!=-1) {
				emoFound=true;
				result = tempEmoType;
				if (display) {
					CalculationWindow.jTextArea.append(String.format("Last emoticon of the input: "+word.elementAt(i)+"\n"));
					CalculationWindow.jTextArea.append(String.format("Emotion type: "+ Constant.differentTypeOfEmo[tempEmoType]+"\n"));
				}
				break;
			}
		}
	}
	
	public boolean isEmoFound() {
		return emoFound;
	}
	
	public int getResult() {
		return result;
	}
	
	//return the type of the emoticon from the database
	// if no emoticon is found return -1
	private int getEmoType(String emo) {
		int ret =-1;

		try {
			//connection= DriverManager.getConnection(URL, USER, PASSWORD);
			connection = ConnectionClassTEAapi.getTEAConnection();
			String query = "select e.emo_type from emo e where e.emo= ?";
			preparedStatement= connection.prepareStatement(query);
			preparedStatement.setString(1, emo);
			resultSet= preparedStatement.executeQuery();
			if (resultSet.next()) {
				ret= resultSet.getInt(1);
			}
			
			//close the connections
			resultSet.close();
			preparedStatement.close();
			//connection.close();
		} catch (SQLException e) {
			System.out.println("error: "+e);
			e.printStackTrace();
		}


		return ret;
	}



}
