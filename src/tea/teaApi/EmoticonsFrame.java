package tea.teaApi;
import java.awt.Button;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;



public class EmoticonsFrame extends JFrame {

	final int ROW=4;
	final int COL=5;
	
	JButton imageButton[][];
	public EmoticonsFrame() {
		
		super("Emoticons");
		setLayout(new GridLayout(ROW,COL));
		
		
		imageButton= new JButton [ROW][COL];
		String imagePath="emoticons/";
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COL; j++) {
				Icon emoIcon=new ImageIcon(imagePath+Integer.toString(i)+Integer.toString(j)+".jpg");
				imageButton[i][j]=new JButton("",emoIcon);
				add(imageButton[i][j]);
			}
		}
		
	}
	
}
