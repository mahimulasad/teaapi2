package tea.teaApi;
import java.util.HashMap;
import java.util.Map;


public class EmotionOfImage {
	
	final static int NEUTRAL=0;
	final static int ANGER=1;
	final static int DISGUST=2;
	final static int FEAR=3;
	final static int GUILT=4;
	final static int INTEREST=5;
	final static int JOY=6;
	final static int SAD=7;
	final static int SHAME=8;
	final static int SURPRISE=9;
	final static int MIXED=10;
	
	private String path="emoticons/";
	private String name[]={
			"00","01","02","03","04",
			"10","11","12","13","14",
			"20","21","22","23","24",
			"30","31","32","33","34"
			};
	private int emotions[]={
			JOY,JOY,JOY,SAD,NEUTRAL,
			SAD,JOY,SAD,SAD,SURPRISE,
			JOY,JOY,JOY,JOY,JOY,
			SAD,SAD,JOY,ANGER,ANGER
		};
	Map<String, Integer>emoOfImage;
	
	public EmotionOfImage() {
		
		emoOfImage= new HashMap<String, Integer>();
		for (int i = 0; i < name.length; i++) {
			String fullName= path+name[i]+".jpg";
			emoOfImage.put(fullName, emotions[i]);
		}
		
	}
	
	public int getEmotionOfTheImage(String s) {
		return emoOfImage.get(s);
	}
}
