package tea.teaApi;

/*
 * This code is not necessary  
 * in current version of the system
 */

@Deprecated
public class HMM {

	final static int NEUTRAL=0;
	final static int ANGER=1;
	final static int DISGUST=2;
	final static int FEAR=3;
	final static int GUILT=4;
	final static int INTEREST=5;
	final static int JOY=6;
	final static int SAD=7;
	final static int SHAME=8;
	final static int SURPRISE=9;
	final static int MIXED=10;
	
	static int table[][]={
			
			{NEUTRAL,ANGER,SURPRISE,MIXED,MIXED,MIXED,JOY,NEUTRAL,MIXED,MIXED,MIXED},
			{ANGER,ANGER,ANGER,MIXED,MIXED,ANGER,MIXED,SAD,MIXED,ANGER,ANGER},
			{DISGUST,ANGER,ANGER,MIXED,MIXED,DISGUST,JOY,SAD,MIXED,DISGUST,DISGUST},
			{FEAR,MIXED,MIXED,FEAR,NEUTRAL,NEUTRAL,MIXED,SAD,MIXED,MIXED,FEAR},
			{GUILT,GUILT,GUILT,MIXED,GUILT,MIXED,MIXED,SAD,MIXED,MIXED,GUILT},
			{INTEREST,ANGER,ANGER,MIXED,MIXED,INTEREST,JOY,SAD,MIXED,JOY,MIXED},
			{JOY,MIXED,ANGER,MIXED,MIXED,JOY,JOY,SAD,MIXED,JOY,JOY,MIXED},
			{SAD,SAD,SAD,MIXED,SAD,SAD,SAD,SAD,SAD,MIXED,SAD},
			{SHAME,SHAME,SHAME,NEUTRAL,SHAME,MIXED,MIXED,MIXED,SHAME,MIXED,MIXED},
			{SURPRISE,NEUTRAL,NEUTRAL,NEUTRAL,MIXED,JOY,JOY,MIXED,MIXED,SURPRISE,SURPRISE},
			{MIXED,MIXED,MIXED,MIXED,MIXED,JOY,JOY,SAD,MIXED,JOY,MIXED}
			
	};
	
	private int user;
	
	
	public HMM(int user) {
		this.user=user;
	}
	
	public int getResult(){
		if (ChatDB.chatUser.size()==0) {
			return NEUTRAL;
		}
		else if(ChatDB.chatUser.elementAt(ChatDB.chatUser.size()-1) ==user){
			return ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-1);
		}
		else if (ChatDB.chatUser.size()==1) {
			return ChatDB.chatEmo.elementAt(0);
		}
		else if (ChatDB.chatUser.size()==2) {
			return table[ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-2)][ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-1)];
		}
		else {
			if (ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-1)== ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-3) ) {
				return ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-2);
			}
			else {
				return table[ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-2)][ChatDB.chatEmo.elementAt(ChatDB.chatEmo.size()-1)];
			}
		}
		
	}
}
