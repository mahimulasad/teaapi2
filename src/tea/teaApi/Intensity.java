package tea.teaApi;
public class Intensity {

	private double anger;
	private double disgust;
	private double fear;
	private double guilt;
	private double interest;
	private double joy;
	private double sadness;
	private double shame;
	private double surprise;

	public Intensity() {

		anger = disgust = fear = guilt = interest = joy = sadness = shame = surprise = 0.0;

	}
	
	public Intensity(double anger, double disgust, double fear, double guilt,
			double interest, double joy, double sadness, double shame,
			double surprise) {

		this.anger = anger;
		this.disgust = disgust;
		this.fear = fear;
		this.guilt = guilt;
		this.interest = interest;
		this.joy = joy;
		this.sadness = sadness;
		this.shame = shame;
		this.surprise = surprise;

	}

	public double getAnger() {
		return anger;
	}

	public void setAnger(double anger) {
		this.anger = anger;
	}

	public double getDisgust() {
		return disgust;
	}

	public void setDisgust(double disgust) {
		this.disgust = disgust;
	}

	public double getFear() {
		return fear;
	}

	public void setFear(double fear) {
		this.fear = fear;
	}

	public double getGuilt() {
		return guilt;
	}

	public void setGuilt(double guilt) {
		this.guilt = guilt;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public double getJoy() {
		return joy;
	}

	public void setJoy(double joy) {
		this.joy = joy;
	}

	public double getSadness() {
		return sadness;
	}

	public void setSadness(double sadness) {
		this.sadness = sadness;
	}

	public double getShame() {
		return shame;
	}

	public void setShame(double shame) {
		this.shame = shame;
	}

	public double getSurprise() {
		return surprise;
	}

	public void setSurprise(double surprise) {
		this.surprise = surprise;
	}
	
	public Intensity convertIntensity(double percent) {
		Intensity temp = new Intensity();
		temp.setAnger( percentCalculator(this.anger, percent));
		temp.setDisgust( percentCalculator(this.disgust, percent));
		temp.setFear( percentCalculator(this.fear, percent));
		temp.setGuilt( percentCalculator(this.guilt, percent));
		temp.setInterest( percentCalculator(this.interest, percent));
		temp.setJoy( percentCalculator(this.joy, percent));
		temp.setSadness( percentCalculator(this.sadness, percent));
		temp.setShame( percentCalculator(this.shame, percent));
		temp.setSurprise( percentCalculator(this.surprise, percent));
		
		return temp;
	}
	
	private double percentCalculator(double type, double percent) {
		return type * percent /100;
	}
	
	public static Intensity add(Intensity a, Intensity b) {
		Intensity temp = new Intensity();
		temp.anger = a.anger + b.anger;
		temp.disgust = a.disgust + b.disgust;
		temp.fear = a.fear + b.fear;
		temp.guilt = a.guilt + b.guilt;
		temp.interest = a.interest+ b.interest;
		temp.joy = a.joy + b.joy;
		temp.sadness = a.sadness + b.sadness;
		temp.shame = a.shame + b.shame;
		temp.surprise = a.surprise + b.surprise;
		
		return temp;
	}
	
	
}
