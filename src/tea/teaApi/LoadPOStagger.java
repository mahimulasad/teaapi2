package tea.teaApi;

import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class LoadPOStagger {
	
	public static MaxentTagger maxentTagger;
	public static Morphology morphology;
	
	public LoadPOStagger() {
		maxentTagger= new MaxentTagger("tagger/english-left3words-distsim.tagger");
		morphology= new Morphology();
		
	}

}
