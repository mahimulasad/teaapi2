package tea.teaApi;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;



public class Main implements ActionListener, Runnable {

    private static final String HOST = "127.0.0.1";
    private static final int PORT = 12345;
    private final JFrame frame = new JFrame();
    private final JTextField textField = new JTextField(25);
    //private final JTextArea ta = new JTextArea(15, 25);
    private StyledDocument document= new DefaultStyledDocument();
    private SimpleAttributeSet simpleAttributeSet= new SimpleAttributeSet();
    private SimpleAttributeSet resultAttributeSet= new SimpleAttributeSet();
    private JScrollPane chatboxJScrollPane;
	private JTextPane chatHistory;
	//private String lastMessage= "a";
    private final JButton send = new JButton("Send");
    private final JButton addEmoticons= new JButton("Add emoticons");
    private final JButton analyze= new JButton("Analyze");
    private final JButton suggestionButton= new JButton("Suggestion");
    private JPanel buttonPanel= new JPanel();
    private JPanel secondLine= new JPanel();
    private volatile PrintWriter out;
    private Scanner cin;
    private Thread thread;
    private Kind kind;
    private String differentTypeOfEmo[] = { "Neutral", "Anger", "Disgust",
			"Fear", "Guilt", "Interest", "Joy", "Sad", "Shame", "Surprise",
			"Mixed" };
	private JPanel displayPanel;
	private JPanel centerPanel;


    public static enum Kind {

        Client(100, "Trying","Mahim",1), Server(700, "Awaiting","Nadia",2);
        private int offset;
        private String activity;
        private String name;
        private int serial;

        private Kind(int offset, String activity,String name,int serial) {
            this.offset = offset;
            this.activity = activity;
            this.name=name;
            this.serial=serial;
        }
    }

    public Main(final Kind kind) {
        this.kind = kind;
        if (kind==Kind.Client) {
        	frame.setTitle(Kind.Server.name+" - Online");
		}
        else {
        	frame.setTitle(Kind.Client.name+" - Online");
		}
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getRootPane().setDefaultButton(send);
        frame.add(textField, BorderLayout.NORTH);
        //frame.add(new JScrollPane(ta), BorderLayout.CENTER);
        //frame.add(send, BorderLayout.SOUTH);
        frame.setLocation(kind.offset, 150);
        frame.setSize(450,450);
       
        send.addActionListener(this);
//        ta.setLineWrap(true);
//        ta.setWrapStyleWord(true);
        //simpleAttributeSet.addAttribute(StyleConstants.CharacterConstants.Foreground, Color.BLACK);
        resultAttributeSet.addAttribute(StyleConstants.CharacterConstants.Foreground, Color.RED);
        try {
			document.insertString(document.getLength(), "", simpleAttributeSet);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
        
		chatHistory= new JTextPane(document);
		chatHistory.setEditable(false);
		chatboxJScrollPane= new JScrollPane(chatHistory,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		centerPanel= new JPanel();
		displayPanel= new JPanel();
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(chatboxJScrollPane,BorderLayout.CENTER);
		centerPanel.add(displayPanel,BorderLayout.EAST);
		frame.add(centerPanel,BorderLayout.CENTER);
		
		buttonPanel.setLayout(new GridLayout(2, 1));
		buttonPanel.add(send);
		secondLine.setLayout(new GridLayout(1,3));
		secondLine.add(addEmoticons);
		secondLine.add(analyze);
		secondLine.add(suggestionButton);
		buttonPanel.add(secondLine);
		
		frame.add(buttonPanel, BorderLayout.SOUTH);
		
		addEmoticons.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				EmoticonsFrame emoticonsFrame = new EmoticonsFrame("");
				emoticonsFrame.setSize(175,180);
				emoticonsFrame.setVisible(true);
				
			}
		});
		
		analyze.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO edit
				/*******************************
				 * place in other block
				 * 
				 */
				//System.out.println("last msg: "+TempData.lastMessage);
				new CalculationWindow(TempData.lastMessage);
				
//				int temp;
//				String displayName;
//				if (kind==Kind.Client) {
//					displayName= Kind.Server.name;
//					temp=2;
//				}
//				else {
//					displayName= Kind.Client.name;
//					temp=1;
//				}
//				HMM hmm= new HMM(temp);
//				int result= hmm.getResult();
//				JOptionPane.showMessageDialog(null, "Next probable state of "+displayName+": "+differentTypeOfEmo[result],"Next probable state",JOptionPane.INFORMATION_MESSAGE);
				
			}
		});
		
		suggestionButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
			
				SuggestionNB suggestionFrame= new SuggestionNB("");
			}
		});
		
		//frame.pack();
//        DefaultCaret caret = (DefaultCaret) ta.getCaret();
//        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
       // display("",kind.activity + HOST + " on port " + PORT);
        thread = new Thread(this, kind.toString());
    }

    public void start() {
        frame.setVisible(true);
        thread.start();
    }

    //@Override
    public void actionPerformed(ActionEvent ae) {
        String s = textField.getText().toString();
        TempData.lastMessage= s;
        //System.out.println("last msg: "+lastMessage);
        ChatDB.addSentenceToDB(kind.serial, s);
        if (out != null) {
            out.println(s);
        }
        display("Me",s);
        textField.setText("");
    }

    //@Override
    public void run() {
        try {
            Socket socket;
            if (kind == Kind.Client) {
                socket = new Socket(HOST, PORT);
            } else {
                ServerSocket ss = new ServerSocket(PORT);
                socket = ss.accept();
            }
            cin = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream(), true);
            //display("Server","Connected");
            while (true) {
            	Kind tempKind= (kind==Kind.Server)?Kind.Client:Kind.Server;
            	String temp= String.format("%s", tempKind.name);
                display(temp,cin.nextLine());
            }
        } catch (Exception e) {
            //display(kind,e.getMessage());
            e.printStackTrace(System.err);
        }
    }

    private void display(final String name, final String s) {
        EventQueue.invokeLater(new Runnable() {
            //@Override
            public void run() {
                //ta.append(s + "\u23CE\n");
            	
				StringTokenizer stringTokenizer = new StringTokenizer(s);
				String tokenizeWord = "";
				if (stringTokenizer.hasMoreTokens()) {
					tokenizeWord = stringTokenizer.nextToken();
				}

				if (tokenizeWord.equals("##displayImage##")) {
					tokenizeWord = stringTokenizer.nextToken();
					try {
						document.insertString(document.getLength(),String.format("%s: ", name),simpleAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					StyleContext context = new StyleContext();
					Style labelStyle = context
							.getStyle(StyleContext.DEFAULT_STYLE);

					Icon img = new ImageIcon(tokenizeWord);
					JLabel label = new JLabel(img);
					StyleConstants.setComponent(labelStyle, label);

					try {
						document.insertString(document.getLength(),tokenizeWord, labelStyle);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					try {
						document.insertString(document.getLength(), " \u23CE\n",
								simpleAttributeSet);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
					try {
        				//System.out.println(s);
						
        				int temp= (new EmotionOfImage()).getEmotionOfTheImage(tokenizeWord);
    					try {
    						document.insertString(document.getLength(), "[[Emotion of the text : "+differentTypeOfEmo[temp]+"]]\n", resultAttributeSet);
    					} catch (BadLocationException e) {
    						e.printStackTrace();
    					}
    					
    					context= new StyleContext();
    					labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
    					
    					
    					String imageName= differentTypeOfEmo[temp];
    					if (imageName.equals("Mixed")) {
    						imageName= new String("Neutral");
    						//System.out.println(imageName);
    					}
    					
    					img= new ImageIcon(new String("images/"+imageName+".jpg"));
    					label= new JLabel(img);
    					StyleConstants.setComponent(labelStyle, label);
    					
    					try {
    						document.insertString(document.getLength(), "image of "+imageName, labelStyle);
    					} catch (BadLocationException e) {
    						e.printStackTrace();
    					}
    					try {
    						document.insertString(document.getLength(), "\n\n", resultAttributeSet);
    					} catch (BadLocationException e) {
    						e.printStackTrace();
    					}

					} catch (Exception e) {
						System.out.println(e);
					}
				} 
				else {
        			try {
    					document.insertString(document.getLength(), name+": "+s+" \u23CE\n", simpleAttributeSet);
    				} catch (BadLocationException e) {
    					e.printStackTrace();
    				}
        			try {
        				//System.out.println(s);
        				//int temp= (new SenTexProcess(s)).getResult();
        				//TODO edit
        				Analyzer analyzer= new Analyzer(s,false);
        				int temp= analyzer.getResult();
        				double level= analyzer.getMaxIntensity();
        				//TODO edit
        				//System.out.println(level);
        				displayLevel(temp, level);
    					try {
    						document.insertString(document.getLength(), "[[Emotion of the text : "+differentTypeOfEmo[temp]+"]]\n", resultAttributeSet);
    					} catch (BadLocationException e) {
    						e.printStackTrace();
    					}
    					
    					StyleContext context= new StyleContext();
    					Style labelStyle = context.getStyle(StyleContext.DEFAULT_STYLE);
    					
    					
    					String imageName= differentTypeOfEmo[temp];
    					if (imageName.equals("Mixed")) {
    						imageName= new String("Neutral");
    					}
    					
    					Icon img= new ImageIcon(new String("images/"+imageName+".jpg"));
    					JLabel label= new JLabel(img);
    					StyleConstants.setComponent(labelStyle, label);
    					
    					try {
    						document.insertString(document.getLength(), "image of "+imageName, labelStyle);
    					} catch (BadLocationException e) {
    						e.printStackTrace();
    					}
    					try {
    						document.insertString(document.getLength(), "\n\n", resultAttributeSet);
    					} catch (BadLocationException e) {
    						e.printStackTrace();
    					}
    					//System.out.println("test1 "+kind.toString());
    					if (TempData.foundWordForAnalyze==false && kind==Kind.Server) {
    						//System.out.println("test2");
    						//System.out.println(kind==Kind.Server);
    						//System.out.println(kind==Kind.Client);
    						int automaticSuggestion = JOptionPane.showConfirmDialog(null, "No words in the database!\nDo you want to give suggestion?","Permission",JOptionPane.YES_NO_CANCEL_OPTION);
    						if (automaticSuggestion==0) {
    							new SuggestionNB(s);
							}
						}

					} catch (Exception e) {
						System.out.println(e);
						
					}
   				}
                
            }
        });
    }

    
    //main method
    public static void main(String[] args) {
    	
    	
    	MakeMap makeMap= new MakeMap();
    	ChatDB chatDB= new ChatDB();
    	new NBdata();
    	//new TempData();
    	
        EventQueue.invokeLater(new Runnable() {
            //@Override
            public void run() {
                new Main(Kind.Server).start();
                new Main(Kind.Client).start();
            }
        });
    }
    
    private void displayLevel(int type, double level) {
    	
    	//System.out.println(type+" "+level);
		
    	Graphics graphics=displayPanel.getGraphics();
		int width=displayPanel.getWidth();
		int height=displayPanel.getHeight();
		//System.out.println(width+" "+height);
		
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, width, height);
		
		if (type== Constant.JOY || type== Constant.INTEREST || type== Constant.SURPRISE) {
			graphics.setColor(Color.BLUE);
		}
		else if (type== Constant.NEUTRAL || type== Constant.MIXED) {
			graphics.setColor(Color.WHITE);
		}
		else {
			graphics.setColor(Color.RED);
		}
		
		int x= (int)(level*height/2.0);
		graphics.fillRect(0, height-x+1, width, x);
		//graphics.fillRect(0, 0, width, height);
		
	}
    

    //inner class
    private class EmoticonsFrame extends JFrame {

		public String userName;
		
		final int ROW=4;
		final int COL=5;
		
		JButton imageButton[][];
		
		Map<JButton, String> mapEmoName;
		public EmoticonsFrame(String userNameParameter) {
			
			super("Emoticons");
			
			this.userName=userNameParameter;
			
			setLayout(new GridLayout(ROW,COL));
			
			mapEmoName= new HashMap<JButton,String>();
			
			imageButton= new JButton [ROW][COL];
			String imagePath="emoticons/";
			for (int i = 0; i < ROW; i++) {
				for (int j = 0; j < COL; j++) {
					String temp=imagePath+Integer.toString(i)+Integer.toString(j)+".jpg";
					Icon emoIcon=new ImageIcon(temp);
					imageButton[i][j]=new JButton("",emoIcon);
					mapEmoName.put(imageButton[i][j], temp);
					add(imageButton[i][j]);
					imageButton[i][j].addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent actionEvent) {
							
							String imageNameString= mapEmoName.get(actionEvent.getSource());
							//System.out.println(imageNameString+" "+kind.name);
							ChatDB.addEmoToDB(kind.serial, imageNameString);
							//System.out.println(imageNameString);
							String s = "##displayImage## "+imageNameString;
					        if (out != null) {
					            out.println(s);
					        }
					        display("Me",s);

							setVisible(false);
							
						}
					});
				}
			}
			
		}
		
	}
        
}