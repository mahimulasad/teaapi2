package tea.teaApi;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.swing.JOptionPane;


public class MakeMap {
	
	public static Map<String, Integer> mapEmoAndAbbr;
	public static Map<String, Intensity> mapIntensity;
	public static Map<String, Double> mapCoefficient;
	
	private final int NOUN = 11;
	private final int ADJECTIVE = 12;
	private final int ADVERB = 13;
	private final int VERB = 14;
	private final int INTERJECTION = 15;
	
	private final int EMOTICON = 21;
	private final int ABBREVIATION = 22;
	
	private String filepath="database/";

	public MakeMap() {

		mapIntensity = new HashMap<String, Intensity>();

		makeMapOfPartsOfSpeech("noun.txt", NOUN);

		makeMapOfPartsOfSpeech("adverb.txt", ADVERB);

		makeMapOfPartsOfSpeech("adjective.txt", ADJECTIVE);

		makeMapOfPartsOfSpeech("comparativeDegreeAdjective.txt", ADJECTIVE);

		makeMapOfPartsOfSpeech("superlativeDegreeAdjective.txt", ADJECTIVE);

		makeMapOfPartsOfSpeech("interjection.txt", INTERJECTION);

		makeMapOfPartsOfSpeech("verb.txt", VERB);

		makeMapOfPartsOfSpeech("verb_ing.txt", VERB);

		makeMapOfPartsOfSpeech("verb_pp.txt", VERB);
		
		makeMapOfPartsOfSpeech("suggestion.txt", 0);

		mapEmoAndAbbr = new HashMap<String, Integer>();

		makeMapOfEmoAndAbbr("emoticon.txt", EMOTICON);

		makeMapOfEmoAndAbbr("abbreviation.txt", ABBREVIATION);

		mapCoefficient = new HashMap<String, Double>();

		makeMapOfCoefficient("modifier.txt");

	}
	
	private void makeMapOfPartsOfSpeech(String filename, int partsOfSpeech) {

		File inputStream;
		try {
			
			inputStream = new File(filepath+filename);

			Scanner cin = new Scanner(inputStream);

			while (cin.hasNext()) {
				String string = (String) cin.next();
				double anger = cin.nextDouble();
				double disgust = cin.nextDouble();
				double fear = cin.nextDouble();
				double guilt = cin.nextDouble();
				double interest = cin.nextDouble();
				double joy = cin.nextDouble();
				double sadness = cin.nextDouble();
				double shame = cin.nextDouble();
				double surprise = cin.nextDouble();
				Intensity intensity = new Intensity(anger, disgust, fear,
						guilt, interest, joy, sadness, shame, surprise);
				mapIntensity.put(string, intensity);
			}
			// result+=String.format("%s %d\n", filename,count);
		} catch (Exception e) {
			// result+=String.format("error in file %s\n",filename);
			JOptionPane.showMessageDialog(null,"error in file: "+filename);
		}

	}

	//
	private void makeMapOfEmoAndAbbr(String filename, int type) {

		File inputStream;
		try {
			
			inputStream= new File(filepath+filename);
			
			Scanner cin = new Scanner(inputStream);

			while (cin.hasNext()) {
				String string = (String) cin.next();
				int emoType = cin.nextInt();
				mapEmoAndAbbr.put(string, emoType);
			}
			// result+=String.format("%s %d\n", filename,count);
		} catch (Exception e) {
			// result+=String.format("error in file %s\n",filename);
			JOptionPane.showMessageDialog(null,"error in file: "+filename);
		}

	}

	private void makeMapOfCoefficient(String filename) {

		File inputStream;
		try {
			
			inputStream= new File(filepath+filename);
			
			Scanner cin = new Scanner(inputStream);

			while (cin.hasNext()) {
				String string = (String) cin.next();
				double coefficient = cin.nextDouble();
				mapCoefficient.put(string, coefficient);
			}
			// result+=String.format("%s %d\n", filename,count);
		} catch (Exception e) {
			// result+=String.format("error in file %s\n",filename);
			JOptionPane.showMessageDialog(null,"error in file: "+filename);
		}

	}
	
}
