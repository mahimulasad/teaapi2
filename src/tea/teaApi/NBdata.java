package tea.teaApi;
import java.util.HashMap;
import java.util.Map;


public class NBdata {
	
	public static Count NBcount;
	public static Map<String, Count> NBmap;
	
	public NBdata() {
		
		NBcount= new Count();
		NBmap= new HashMap<String, Count>();
		//NBmap.put("truck", new Count());
		
	}
	
	public static void updateCount(int type, int number){
		
		if (type== Constant.ANGER) {
			NBcount.anger += number;
		}
		else if (type== Constant.DISGUST) {
			NBcount.disgust +=number;
		}
		else if (type== Constant.FEAR) {
			NBcount.fear +=number;
		}
		else if (type== Constant.GUILT) {
			NBcount.guilt +=number;
		}
		else if (type== Constant.INTEREST) {
			NBcount.interest +=number;
		}
		else if (type== Constant.JOY) {
			NBcount.joy +=number;
		}
		else if (type== Constant.SADNESS) {
			NBcount.sadness +=number;
		}
		else if (type== Constant.SHAME) {
			NBcount.shame +=number;
		}
		else if (type== Constant.SURPRISE) {
			NBcount.surprise +=number;
		}
		
		
	}
	
	public static void updateMap(String word, int type){
		
		Count count;
		if (NBmap.containsKey(word)) {
			count= NBmap.get(word);
		}
		else {
			count= new Count();
		}
		
		if (type== Constant.ANGER) {
			count.anger++;
		}
		else if (type== Constant.DISGUST) {
			count.disgust++;
		}
		else if (type== Constant.FEAR) {
			count.fear++;
		}
		else if (type== Constant.GUILT) {
			count.guilt++;
		}
		else if (type== Constant.INTEREST) {
			count.interest++;
		}
		else if (type== Constant.JOY) {
			count.joy++;
		}
		else if (type== Constant.SADNESS) {
			count.sadness++;
		}
		else if (type== Constant.SHAME) {
			count.shame++;
		}
		else if (type== Constant.SURPRISE) {
			count.surprise++;
		}
		
		NBmap.put(word, count);
	}
	
}
