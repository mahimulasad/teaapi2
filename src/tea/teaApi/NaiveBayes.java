package tea.teaApi;
import java.util.Vector;


public class NaiveBayes {
	
	private String input;
	private int type;
	private double level;
	private Vector<Data> temp;
	private Vector<Data> list;
	
	
	public NaiveBayes(String input, int type, int level) {
		
		this.input= input;
		this.type= type;
		this.level= (double)level/100;
		LoadPOStagger loadPOStagger = new LoadPOStagger();
		
	}
	
	public void process() {
		
		
		temp= (new POStagger(input)).getTaggedInfo();
		
		list= new Vector<Data>();
		for (int i = 0; i < temp.size(); i++) {
			if (validPOS(i)) {
				list.addElement(temp.elementAt(i));
			}
		}
//		for (int i = 0; i < list.size(); i++) {
//			System.out.println(list.elementAt(i).getBaseWord());
//		}
		
		//update total count
		NBdata.updateCount(type, list.size());
		//update count of each count
		for (int i = 0; i < list.size(); i++) {
			NBdata.updateMap(list.elementAt(i).getBaseWord(),type);
		}
		
		for (int i = 0; i < list.size(); i++) {
			calculate(list.elementAt(i).getBaseWord());
		}
		
	}
	
	private boolean validPOS(int indx){
		
		if (temp.elementAt(indx).getBaseWord().equals("be")) {
			return false;
		}
		for (int i = 0; i < Constant.NBacceptedtagSet.length; i++) {
			if (Constant.NBacceptedtagSet[i].equals(temp.elementAt(indx).getPos())) {
				return true;
			}
		}
		return false;
		
	}
	
	private void calculate(String word) {
		
		int total= (new Count(NBdata.NBcount)).totalCount();
		
		double pAnger= (double)(NBdata.NBcount.anger)/total;
		double pDisgust= (double)(NBdata.NBcount.disgust)/total;
		double pFear= (double)(NBdata.NBcount.fear)/total;
		double pGuilt= (double)(NBdata.NBcount.guilt)/total;
		double pInterest= (double)(NBdata.NBcount.interest)/total;
		double pJoy= (double)(NBdata.NBcount.joy)/total;
		double pSadness= (double)(NBdata.NBcount.sadness)/total;
		double pShame= (double)(NBdata.NBcount.shame)/total;
		double pSurprise= (double)(NBdata.NBcount.surprise)/total;
		
		//temporary print
		//TODO remove
//		System.out.println(total);
//		System.out.print("count all: "+NBdata.NBcount.anger+ " "+NBdata.NBcount.disgust+ " "+NBdata.NBcount.fear+ " "+NBdata.NBcount.guilt+ " "+NBdata.NBcount.interest+ " "+NBdata.NBcount.joy+ " "+NBdata.NBcount.sadness+ " "+NBdata.NBcount.shame+ " "+NBdata.NBcount.countSurprise);
//		System.out.println();
//		System.out.println("probability 1st: "+pAnger+" "+pDisgust+" "+pFear+" "+pGuilt+" "+pInterest+" "+pJoy+" "+pSadness+" "+pShame+" "+pSurprise);
//		for (String it : NBdata.NBmap.keySet()) {
//			System.out.println(it+" "+NBdata.NBmap.get(it));
//		}
		
		double pWordAnger, pWordDisgust, pWordFear, pWordGuilt, pWordInterest, pWordJoy, pWordSadness, pWordShame, pWordSurprise;
		//System.out.println(word);
		try {
			//System.out.println("test1 "+NBdata.NBcount.anger);
			//System.out.println("test2 "+NBdata.NBmap.get("no").anger);
			pWordAnger= (NBdata.NBmap.get(word).anger)/(NBdata.NBcount.anger);
			pWordAnger= (double)(NBdata.NBmap.get(word).anger)/(NBdata.NBcount.anger);
			pAnger *= pWordAnger;
		} catch (Exception e) {//both for arithmatic exception and null pointer exception for map
			pAnger=0.0;
		}
		
		try {
			//System.out.println("test2 "+NBdata.NBmap.get(word).disgust+" "+NBdata.NBcount.disgust);
			pWordDisgust= (NBdata.NBmap.get(word).disgust)/(NBdata.NBcount.disgust);
			pWordDisgust= (double)(NBdata.NBmap.get(word).disgust)/(NBdata.NBcount.disgust);
			pDisgust *= pWordDisgust;
		} catch (Exception e) {
			pDisgust=0.0;
		}
		
		try {
			pWordFear= (NBdata.NBmap.get(word).fear)/(NBdata.NBcount.fear);
			pWordFear= (double)(NBdata.NBmap.get(word).fear)/(NBdata.NBcount.fear);
			pFear *= pWordFear;
		} catch (Exception e) {
			pFear=0.0;
		}
		
		try {
			pWordGuilt= (NBdata.NBmap.get(word).guilt)/(NBdata.NBcount.guilt);
			pWordGuilt= (double)(NBdata.NBmap.get(word).guilt)/(NBdata.NBcount.guilt);
			pGuilt *= pWordGuilt;
		} catch (Exception e) {
			pGuilt=0.0;
		}
		
		try {
			pWordInterest= (NBdata.NBmap.get(word).interest)/(NBdata.NBcount.interest);
			pWordInterest= (double)(NBdata.NBmap.get(word).interest)/(NBdata.NBcount.interest);
			pInterest *= pWordInterest;
		} catch (Exception e) {
			pInterest=0.0;
		}
		
		try {
			pWordJoy= (NBdata.NBmap.get(word).joy)/(NBdata.NBcount.joy);
			pWordJoy= (double)(NBdata.NBmap.get(word).joy)/(NBdata.NBcount.joy);
			pJoy *= pWordJoy;
		} catch (Exception e) {
			pJoy=0.0;
		}
		
		try {
			pWordSadness= (NBdata.NBmap.get(word).sadness)/(NBdata.NBcount.sadness);
			pWordSadness= (double)(NBdata.NBmap.get(word).sadness)/(NBdata.NBcount.sadness);
			pSadness *= pWordSadness;
		} catch (Exception e) {
			pSadness=0.0;
		}
		
		try {
			pWordShame= (NBdata.NBmap.get(word).shame)/(NBdata.NBcount.shame);
			pWordShame= (double)(NBdata.NBmap.get(word).shame)/(NBdata.NBcount.shame);
			pShame *= pWordShame;
		} catch (Exception e) {
			pShame=0.0;
		}
		
		try {
			pWordSurprise= (NBdata.NBmap.get(word).surprise)/(NBdata.NBcount.surprise);
			pWordSurprise= (double)(NBdata.NBmap.get(word).surprise)/(NBdata.NBcount.surprise);
			pSurprise *= pWordSurprise;
		} catch (Exception e) {
			pSurprise=0.0;
		}
		
		//TODO remove this temporary print line
		//System.out.println("probability last: "+pAnger+" "+pDisgust+" "+pFear+" "+pGuilt+" "+pInterest+" "+pJoy+" "+pSadness+" "+pShame+" "+pSurprise);
		
		
		double highestProb=0.0;
		
		//getting highest probability
		if (pAnger>highestProb) {
			highestProb= pAnger;
		}
		
		if (pDisgust>highestProb) {
			highestProb= pDisgust;
		}
		
		if (pFear>highestProb) {
			highestProb= pFear;
		}
		
		if (pGuilt>highestProb) {
			highestProb= pGuilt;
		}
		
		if (pInterest>highestProb) {
			highestProb= pInterest;
		}
		
		if (pJoy>highestProb) {
			highestProb= pJoy;
		}
		
		if (pSadness>highestProb) {
			highestProb= pSadness;
		}
		
		if (pShame>highestProb) {
			highestProb= pShame;
		}
		
		if (pSurprise>highestProb) {
			highestProb= pSurprise;
		}
		
		//checking if multiple highest value exists
		boolean tempType[]= new boolean[11];
		
		if (pAnger==highestProb) {
			tempType[Constant.ANGER]=true;
		}
		
		if (pDisgust==highestProb) {
			tempType[ Constant.DISGUST]=true;
		}
		
		if (pFear==highestProb) {
			tempType[ Constant.FEAR]=true;
		}
		
		if (pGuilt==highestProb) {
			tempType[ Constant.GUILT]=true;
		}
		
		if (pInterest==highestProb) {
			tempType[ Constant.INTEREST]=true;
		}
		
		if (pJoy==highestProb) {
			tempType[ Constant.JOY]=true;
		}
		
		if (pSadness==highestProb) {
			tempType[Constant.SADNESS]=true;
		}
		
		if (pShame==highestProb) {
			tempType[ Constant.SHAME]=true;
		}
		
		if (pSurprise==highestProb) {
			tempType[ Constant.SURPRISE]=true;
		}
		
		(new WriteSuggestion(word, tempType, level)).writeInFile();		
		
	}
	

}
