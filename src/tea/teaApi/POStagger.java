package tea.teaApi;
import java.util.StringTokenizer;
import java.util.Vector;

import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;


public class POStagger {

	private String inputString;
	private String taggedString;
	private Data tempData;
	private Vector<Data> taggedWordList;
	
	public POStagger(String inputString) {
		
		this.inputString=inputString;
		
		taggedWordList= new Vector<Data>();
		
	}
	
	public Vector<Data> getTaggedInfo() {
		
		taggedString= LoadPOStagger.maxentTagger.tagString(inputString);
		//System.out.println(inputString);
		//System.out.println(taggedString);
		
		StringTokenizer tokenizer= new StringTokenizer(taggedString);
		while (tokenizer.hasMoreTokens()) {
			String temp= tokenizer.nextToken();
			//System.out.println(temp);
			StringTokenizer innerTokenizer= new StringTokenizer(temp,"_");
			tempData= new Data();
			tempData.setMainWord(innerTokenizer.nextToken());
			tempData.setPos(innerTokenizer.nextToken());
			//all the base form of the words will be stored in lower case
			tempData.setBaseWord(LoadPOStagger.morphology.lemma(tempData.getMainWord(), tempData.getPos()).toLowerCase());
			taggedWordList.addElement(tempData);
			//System.out.println(tempData.getMainWord()+" "+tempData.getPos()+" "+tempData.getBaseWord());
		}
		
		return taggedWordList;
	}
	
}
