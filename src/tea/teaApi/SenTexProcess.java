package tea.teaApi;
import java.util.StringTokenizer;
import java.util.Vector;


@Deprecated
public class SenTexProcess {

	private String inputString;
	private Vector<String> word;
	// private int analysisStartIndex;
	private boolean isEmoOrAbbrFound;
	private Vector<String> wordForAnalysis;
	private Vector<Intensity> intensityOfAnalyzedKeyword;
	private Vector<Double> coeffientOfEachWord;
	

	private int result;
	private String keyword;
	private String differentTypeOfEmo[] = { "Neutral", "Anger", "Disgust",
			"Fear", "Guilt", "Interest", "Joy", "Sad", "Shame", "Surprise",
			"Mixed" };

	private final int NUTRAL = 0;
	private final int ANGER = 1;
	private final int DISGUST = 2;
	private final int FEAR = 3;
	private final int GUILT = 4;
	private final int INTEREST = 5;
	private final int JOY = 6;
	private final int SADNESS = 7;
	private final int SHAME = 8;
	private final int SURPRISE = 9;
	private final int MIXED = 10;

	

	

	public SenTexProcess(String inputString) {

		
		
		// save input value
		this.inputString = inputString;

		//
		//result = new String();
		keyword = new String("");

		// parse input string
		sentenceParsing();
		

		// map all the emo and words
		//makeMap();

		//
		isEmoOrAbbrFound = analyzeEmoAndAbbr();
		
		if (isEmoOrAbbrFound == false) {

			//
			analyzeText();

			calculateResult();
		}
		
	}

	private void sentenceParsing() {

		word = new Vector<String>();

		StringTokenizer stringTokenizer = new StringTokenizer(inputString);
		String tokenizeWord;
		while (stringTokenizer.hasMoreTokens()) {
			tokenizeWord = stringTokenizer.nextToken();
			// tokenizeWord=tokenizeWord.toLowerCase();
			tokenizeWord = removePunctuation(tokenizeWord);
			word.addElement(tokenizeWord);
		}
	}

	// remove extra punctuation from the tail of the string
	private String removePunctuation(String str) {

		if (isEmo(str)) {// if the string is a emo, no need to remove anything
			return str;
		} else {
			str = str.toLowerCase();
			int len = str.length();
			for (int i = len - 1; i >= 0; i--) {
				if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
					return str.substring(0, i + 1);
				}
			}
		}
		return "";
	}

	// returns if the string is a emo or not
	private boolean isEmo(String str) {

		if (str.length() == 1) {// is not a emo if its length is 1
			return false;
		}
		str = str.toLowerCase();
		// if any of the 1st two character is not alphabet, then it's a emo
		if (!(str.charAt(0) >= 'a' && str.charAt(0) <= 'z')
				|| !(str.charAt(1) >= 'a' && str.charAt(1) <= 'z')) {
			return true;
		}
		return false;
	}

	//
	

	

	private void analyzeText() {
		sentenceLevelAnalysis();
		wordLevelAnalysis();

	}

	private boolean analyzeEmoAndAbbr() {
		for (int i = word.size() - 1; i >= 0; i--) {
			if (MakeMap.mapEmoAndAbbr.containsKey(word.elementAt(i))) {
				result =MakeMap.mapEmoAndAbbr.get(word.elementAt(i));
				return true;
			}
		}
		return false;
	}

	private void sentenceLevelAnalysis() {

		int analysisStartIndex = word.indexOf("but");
		analysisStartIndex++;

		wordForAnalysis = new Vector<String>();
		coeffientOfEachWord = new Vector<Double>();

		double coefficient = 1.0;

		for (int i = analysisStartIndex; i < word.size(); i++) {
			if (MakeMap.mapIntensity.containsKey(word.elementAt(i))) {
				wordForAnalysis.addElement(word.elementAt(i));
				coeffientOfEachWord.addElement(coefficient);
				// coefficient=1.0;
			}
			if (MakeMap.mapCoefficient.containsKey(word.elementAt(i))) {
				coefficient = MakeMap.mapCoefficient.get(word.elementAt(i));
			} else {
				coefficient = 1.0;
			}
		}

	}

	private void wordLevelAnalysis() {

		intensityOfAnalyzedKeyword = new Vector<Intensity>();
		for (int i = 0; i < wordForAnalysis.size(); i++) {

			Intensity intensity = calculateIntensityMultiplyCoefficient(
					MakeMap.mapIntensity.get(wordForAnalysis.elementAt(i)),
					coeffientOfEachWord.elementAt(i));

			intensityOfAnalyzedKeyword.addElement(intensity);

		}

	}

	private Intensity calculateIntensityMultiplyCoefficient(
			Intensity intensity, double coefficient) {

		if (coefficient == 1.0) {
			return intensity;
		} else if (coefficient > 0.0) {
			Intensity ret = new Intensity();

			ret.setAnger(intensity.getAnger() * coefficient);
			ret.setDisgust(intensity.getDisgust() * coefficient);
			ret.setFear(intensity.getFear() * coefficient);
			ret.setGuilt(intensity.getGuilt() * coefficient);
			ret.setInterest(intensity.getInterest() * coefficient);
			ret.setJoy(intensity.getJoy() * coefficient);
			ret.setSadness(intensity.getSadness() * coefficient);
			ret.setShame(intensity.getShame() * coefficient);
			ret.setSurprise(intensity.getSurprise() * coefficient);

			return ret;
		} else if (coefficient < 0.0) {

			double maxPos = 0.0;
			double maxNeg = 0.0;

			maxPos = Math.max(maxPos, intensity.getJoy());
			maxPos = Math.max(maxPos, intensity.getInterest());
			maxPos = Math.max(maxPos, intensity.getSurprise());

			maxNeg = Math.max(maxNeg, intensity.getSadness());
			maxNeg = Math.max(maxNeg, intensity.getAnger());
			maxNeg = Math.max(maxNeg, intensity.getDisgust());
			maxNeg = Math.max(maxNeg, intensity.getFear());
			maxNeg = Math.max(maxNeg, intensity.getGuilt());
			maxNeg = Math.max(maxNeg, intensity.getShame());

			Intensity ret = new Intensity(0.0, 0.0, 0.0, 0.0, 0.0, maxNeg,
					maxPos, 0.0, 0.0);

			return ret;
		}
		return intensity;
	}

	private void calculateResult() {
		double totalAnger = 0.0;
		double totalDisgust = 0.0;
		double totalFear = 0.0;
		double totalGuilt = 0.0;
		double totalInterest = 0.0;
		double totalJoy = 0.0;
		double totalSadness = 0.0;
		double totalShame = 0.0;
		double totalSurprise = 0.0;

		for (int i = 0; i < intensityOfAnalyzedKeyword.size(); i++) {
			Intensity intensity = intensityOfAnalyzedKeyword.elementAt(i);

			totalAnger += intensity.getAnger();
			totalDisgust += intensity.getDisgust();
			totalFear += intensity.getFear();
			totalGuilt += intensity.getGuilt();
			totalInterest += intensity.getInterest();
			totalJoy += intensity.getJoy();
			totalSadness += intensity.getSadness();
			totalShame += intensity.getShame();
			totalSurprise += intensity.getSurprise();

		}

		double maxPos = 0.0;
		double maxNeg = 0.0;
		int emoPos = 0;
		int emoNeg = 0;

		if (totalJoy > maxPos) {
			maxPos = totalJoy;
			emoPos = JOY;
		}

		if (totalInterest > maxPos) {
			maxPos = totalInterest;
			emoPos = INTEREST;
		}

		if (totalSurprise > maxPos) {
			maxPos = totalSurprise;
			emoPos = SURPRISE;
		}

		if (totalSadness > maxNeg) {
			maxNeg = totalSadness;
			emoNeg = SADNESS;
		}

		if (totalAnger > maxNeg) {
			maxNeg = totalAnger;
			emoNeg = ANGER;
		}

		if (totalDisgust > maxNeg) {
			maxNeg = totalDisgust;
			emoNeg = DISGUST;
		}

		if (totalFear > maxNeg) {
			maxNeg = totalFear;
			emoNeg = FEAR;
		}

		if (totalGuilt > maxNeg) {
			maxNeg = totalGuilt;
			emoNeg = GUILT;
		}
		if (totalShame > maxNeg) {
			maxNeg = totalShame;
			emoNeg = SHAME;
		}

		//
		if (maxPos == 0.0 && maxNeg == 0.0) {
			//result += differentTypeOfEmo[NUTRAL];
			result =NUTRAL;
		} else if (maxPos == maxNeg) {
			result = MIXED;
		} else if (maxPos > maxNeg) {
			result = emoPos;
		} else if (maxNeg > maxPos) {
			result =emoNeg;
		}

	}

	public int getResult() {
		return result;
	}

	public String getKeyWord() {
		Vector<String> keywordVector = new Vector<String>();

		for (int i = 0; i < word.size(); i++) {
			if (MakeMap.mapEmoAndAbbr.containsKey(word.elementAt(i))) {
				keywordVector.addElement(word.elementAt(i));
			} else if (MakeMap.mapIntensity.containsKey(word.elementAt(i))) {
				keywordVector.addElement(word.elementAt(i));
			} else if (MakeMap.mapCoefficient.containsKey(word.elementAt(i))) {
				keywordVector.addElement(word.elementAt(i));
			}
		}

		if (keywordVector.size() == 0) {
			keyword = "No keyword in the text";
		} else if (keywordVector.size() == 1) {
			keyword = "Keyword in the text:\n" + keywordVector.elementAt(0);
		} else {
			keyword = "Keywords in the text:\n";
			for (int i = 0; i < keywordVector.size(); i++) {
				keyword += String.format("%s\n", keywordVector.elementAt(i));
			}
		}

		return keyword;
	}

}