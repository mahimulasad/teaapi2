package tea.teaApi;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;



public class SuggestionNB extends JFrame {
	
	private JTextArea inputJTextArea= new JTextArea(6,20);
	
	private JComboBox<String> type;
	private JComboBox<String> level;
	private JButton ok;
	final JSlider slider;
	
	private String typeName[]= { "Neutral", "Anger", "Disgust",
			"Fear", "Guilt", "Interest", "Joy", "Sad", "Shame", "Surprise",
			"Mixed" };
	private String levelName[]= {"Low", "Medium","High"};
	
	private JLabel inputJLabel;
	private JLabel typejLabel;
	private JLabel levelJLabel;
	

	public SuggestionNB(String suggestion) {
		
		setSize(280, 320);
		setVisible(true);
		setTitle("Suggestion");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		type= new JComboBox<String>(typeName);
		type.setMaximumRowCount(5);
		type.setSelectedIndex(0);
		level= new JComboBox<String>(levelName);
		level.setMaximumRowCount(3);
		level.setSelectedIndex(1);
		ok= new JButton("Ok");
		slider= new JSlider(SwingConstants.HORIZONTAL, 0,100,60);
		slider.setMajorTickSpacing(20);
		//slider.setLabelTable(slider.createStandardLabels(10));
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		
		Dictionary dictionary= new Hashtable<>();
		for (int i = 0; i <= 100; i+=20) {
			dictionary.put(i, new JLabel(Double.toString((double)i/100)));
		}
		slider.setLabelTable(dictionary);
		
		inputJLabel= new JLabel("Enter a sentence:");
		typejLabel= new JLabel("Select an emotion type:");
		levelJLabel= new JLabel("Select intensity level of the sentence:");
		//System.out.println(getHeight()+" "+getWidth());
		
		setLayout(new FlowLayout());
		add(inputJLabel);
		inputJTextArea.setText(suggestion);
		inputJTextArea.setLineWrap(true);
		JScrollPane textAreajJScrollPane= new JScrollPane(inputJTextArea);
		textAreajJScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		textAreajJScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		add(textAreajJScrollPane);
		add(typejLabel);
		add(type);
		add(levelJLabel);
		add(slider);
		add(new JLabel("  "));
		add(ok);
		//pack();
		
		ok.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				//System.out.println(type.getSelectedIndex()+" "+level.getSelectedIndex());
				String tempInput= inputJTextArea.getText().toString();
				if (tempInput.length()==0) {
					JOptionPane.showMessageDialog(SuggestionNB.this,"No input is given", "Error",JOptionPane.ERROR_MESSAGE);
				}
				else {
//					(new WriteSuggestion(tempInput, type.getSelectedIndex(), level.getSelectedIndex())).writeInFile();
//					setVisible(false);
					(new NaiveBayes(tempInput, type.getSelectedIndex(), slider.getValue())).process();
					setVisible(false);
				}
			}
		});
		
		slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				//System.out.println(slider.getValue());
				
			}
		});
	}
	
	
	
	
}
