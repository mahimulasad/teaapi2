package tea.teaApi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class WordType {
	
	private String sequence;
	
	private boolean isEmo;
	private boolean isHashTag;
	private boolean isEmotionalWord;
	private boolean isAbbr;
	private int type;
	
	private final String url= "jdbc:mysql://localhost:3306/teadb";
	private final String user= "root";
	private final String password= "";
	
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;
	
	
	public WordType(String word) {
		this.sequence = word;
//		if (sequence.contains("\'")) {//it isn't needed as we are using prepared statement
//			sequence= sequence.replaceAll("\'", "\'\'");//replace ' with '' for sql operation
//		}

		isEmo=false;
		isHashTag=false;
		isEmotionalWord = false;
		isAbbr = false;
		type = Constant.NEUTRAL;
		
		connection= null;
		preparedStatement= null;
		resultSet= null;
		
		setIsHashTag();
		setIsEmotionalWord();
		
		if(!isHashTag && !isEmotionalWord){
			setIsEmo();
			if (!isEmo) {
				setIsAbbr();
			}
		}
	}

	public boolean isEmo() {
		return isEmo;
	}

	private void setIsEmo() {
//		if (sequence.length() == 1) {// is not a emo if its length is 1
//			isEmo=false;
//		}
//		sequence = sequence.toLowerCase();
//		// if any of the 1st two character is not alphabet, then it's a emo
//		if (!(sequence.charAt(0) >= 'a' && sequence.charAt(0) <= 'z')
//				|| !(sequence.charAt(1) >= 'a' && sequence.charAt(1) <= 'z')) {
//			isEmo=true;
//		}
//		isEmo=false;
		
		try {
			//connection= DriverManager.getConnection(url, user, password);
			connection = ConnectionClassTEAapi.getTEAConnection();
			String query = "select * from emo where emo = ?";//mysql search is case insensetive
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, sequence);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				isEmo= true;
				int emoType = resultSet.getInt("emo_type");
				setType(emoType);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(sequence);
		}
		finally{
			try {
				resultSet.close();
				preparedStatement.close();
				//connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
	}

	public boolean isHashTag() {
		return isHashTag;
	}

	private void setIsHashTag() {
		if (sequence.charAt(0)=='#') {
			isHashTag=true;
		}
	}

	public boolean isEmotionalWord() {
		return isEmotionalWord;
	}

	private void setIsEmotionalWord() {
		String emotionalWord;
		if (isHashTag) {
			emotionalWord= this.sequence.substring(1);
		}
		else {
			emotionalWord = this.sequence;
		}
		
		try {
			//connection= DriverManager.getConnection(url, user, password);
			connection = ConnectionClassTEAapi.getTEAConnection();
			
			String query = "select * from word where word= ?";
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, emotionalWord);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				isEmotionalWord = true;
				emotionalWord= resultSet.getString(1);
				double anger, disgust, fear, guilt, interest, joy, sadness, shame, surprise;
				anger = resultSet.getDouble(2);
				disgust = resultSet.getDouble(3);
				fear = resultSet.getDouble(4);
				guilt = resultSet.getDouble(5);
				interest = resultSet.getDouble(6);
				joy = resultSet.getDouble(7);
				sadness = resultSet.getDouble(8);
				shame = resultSet.getDouble(9);
				surprise = resultSet.getDouble(10);
//				System.out.println(emotionalWord+" "+anger+" "+ disgust+" "
//						+ disgust+ " "+ fear+ " "+ guilt+" "+ interest+" "
//								+ joy+ " "+ sadness+" "+shame+" "+surprise);
				int tempType = maxIntensityType(anger, disgust, fear, guilt, interest, joy, sadness, shame, surprise);
				setType(tempType);
			}
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			//close the connections
			try {
				resultSet.close();
				preparedStatement.close();
				//connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	
	private int maxIntensityType(double anger, double disgust, double fear, double guilt, 
			double interest, double joy, double sadness, double shame, double surprise) {
		
		double max= -1.00;
		int temp = Constant.NEUTRAL;
		if (joy>= max) { //if the value is equal or larger than max
			temp= Constant.JOY;
			max= joy;
		}
		if(interest>max){//if the value is larger than joy or max
			temp= Constant.INTEREST;
			max= interest;
		}
		if (surprise>max) {//if the value is larger than joy or max
			temp = Constant.SURPRISE;
			max = surprise;
		}
		
		
		if (sadness>=max) {//if the value is equal or larger than max
			temp = Constant.SADNESS;
			max = sadness;
		}
		if (anger>max) {//if the value is larger than sadness
			temp= Constant.ANGER;
			max= anger;
		}
		if (disgust> max) {//if the value is larger than sadness
			temp = Constant.DISGUST;
			max = disgust;
		}
		if (fear>max) {//if the value is larger than sadness
			temp = Constant.FEAR;
			max = fear;
		}
		if (guilt>max) {//if the value is larger than sadness
			temp = Constant.GUILT;
			max = guilt;
		}
		if (shame>max) {//if the value is larger than sadness
			temp = Constant.SHAME;
			max = shame;
		}
		
		return temp;
		
	}

	public boolean isAbbr() {
		return isAbbr;
	}

	private void setIsAbbr() {
		try {
			//connection= DriverManager.getConnection(url, user, password);
			connection = ConnectionClassTEAapi.getTEAConnection();
			String query = "select * from abbreviation where abbreviation= ?";
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, sequence);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				isAbbr= true;
				int emoType = resultSet.getInt("abbreviation_type");
				setType(emoType);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				resultSet.close();
				preparedStatement.close();
				//connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public int getType() {
		return type;
	}

	private void setType(int type) {
		this.type = type;
	}
	
	

}
