package tea.teaApi;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Deprecated
public class WriteSuggestion {

	private String name;
	private boolean type[];
	private double level;
	
	private double anger,disgust,fear,guilt,interest,joy,sadness, shame, surprise;
	
	//private String writeString;
	
	//instance variable for handling database
	Connection connection;
	Statement statement;
	
	private final String URL= "jdbc:mysql://localhost:3306/teadb";
	private final String USER= "root";
	private final String PASSWORD= "";
	
	public WriteSuggestion(String name,boolean type[],double level) {
		
		this.name=name;
		this.type=type;
		this.level=level;
		
		anger=disgust=fear=guilt=interest=joy=sadness=shame=surprise=0.0;
		
	}
	
	public void writeInFile() {
		
		calculate();
		
//		try {
//			RandomAccessFile file= new RandomAccessFile("database/suggestion.txt", "rw");
//			file.seek(file.length());
//			//file.writeBytes("\r\n");
//			file.writeBytes(writeString);
//			file.writeBytes("\r\n");
//			file.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		try {
			connection= DriverManager.getConnection(URL, USER, PASSWORD);
			statement= connection.createStatement();
			String queryString= "INSERT INTO SUGGESTION VALUES "
					+ "('"+name+"', '"+anger+"', '"+ disgust+"', '"+ fear+ "', '"+guilt
							+ "', '"+ interest+"', '"+ joy+"', '"+ sadness+
							"', '"+ shame+"', '"+ surprise+"')";
			statement.executeUpdate(queryString);
			
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	private void calculate() {
		//double temp= 0.4+level*0.2;
		
		if (type[1]) {
			anger=level;
		}
		if (type[2]) {
			disgust=level;
		}
		if (type[3]) {
			fear=level;
		}
		if (type[4]) {
			guilt=level;
		}
		if (type[5]) {
			interest=level;
		}
		if (type[6]) {
			joy=level;
		}
		if (type[7]) {
			sadness=level;
		}
		if (type[8]) {
			shame=level;
		}
		if (type[9]) {
			surprise=level;
		}
		if (type[10]) {
			joy=sadness=level;
		}
		
//		writeString= new String(name);
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(anger));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(disgust));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(fear));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(guilt));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(interest));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(joy));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(sadness));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(shame));
//		writeString= writeString.concat(" ");
//		writeString= writeString.concat(Double.toString(surprise));
//		writeString= writeString.concat("\n\n");
		
//		Intensity intensity= new Intensity(anger, disgust, fear, guilt, interest, joy, sadness, shame, surprise);
		//if (!MakeMap.mapIntensity.containsKey(name)) {
//			MakeMap.mapIntensity.put(name, intensity);
			//TODO add NB database
		//}
		
	}

}
