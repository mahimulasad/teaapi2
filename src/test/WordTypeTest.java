package test;

import java.util.Scanner;
import java.util.StringTokenizer;

import tea.teaApi.WordType;

public class WordTypeTest {

	public static void main(String[] args) {
		
		Scanner cin = new Scanner(System.in);
		String input = cin.nextLine();
		StringTokenizer tokenizer = new StringTokenizer(input);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			
			WordType wordType = new WordType(token);
			System.out.println(token);
			System.out.println(wordType.isAbbr()+" "+wordType.isEmo()+" "+wordType.isHashTag()+" "+wordType.isEmotionalWord());
			System.out.println(wordType.getType());
		}
		

	}

}
